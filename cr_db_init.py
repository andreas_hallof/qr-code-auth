#! /usr/bin/env python3

import datetime, os, sys
from peewee import *


CR_DB_FILE="ChallengeResponse.db"

cr_db = SqliteDatabase(CR_DB_FILE)

class BaseModel(Model):
    class Meta:
        database = cr_db

class CR(BaseModel):
    id = PrimaryKeyField()
    DateCreated = DateTimeField(default = datetime.datetime.now)
    ChallengeURL=CharField()
    Response=CharField(default='')
    # bei sqlite ist, wird dies auf integer ge-map-pt, aber egal
    # http://docs.peewee-orm.com/en/latest/peewee/models.html#field-types-table
    ResponseOK=BooleanField(default = False)

def Verbindung_CR_DB():
    cr_db.connect()

def Schliesse_CR_DB():
    cr_db.close()

if __name__ == '__main__':

    Verbindung_CR_DB()

    if CR.table_exists():
        CR.drop_table()
    CR.create_table() # merker alternativ: cr_db.create_tables([CR])

    #for x in Challenge.select():
    #    print(x.id, x.AESChallenge)

    Schliesse_CR_DB()

