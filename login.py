#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import os, sys, binascii, hashlib, io, re, pyqrcode
from flask import Flask, request, redirect, Response, render_template, send_from_directory, jsonify
from base64 import b64decode, b64encode

from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives import serialization, hashes
from cryptography.exceptions import InvalidSignature, InvalidTag

from cr_db_init import *

app = Flask(__name__)
schlüsseldatei="ee_key.pem"; certdatei="ee_cert.pem"; BaseURL=""
MY_DEBUG=False
BaseURL="tiauth.gq/login.py"


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),'favicon.ico', mimetype='image/vnd.microsoft.icon')


def Signatur(dtbs : bytearray):

    with open(schlüsseldatei, "rb") as f:
        serialized_private_sig_key=f.read()

    private_sig_key = serialization.load_pem_private_key(
        serialized_private_sig_key,
        password=None,
        backend=default_backend()
    )

    signature = private_sig_key.sign( dtbs , ec.ECDSA(hashes.SHA256()) )
    sig_transfer_ecc=binascii.hexlify(signature)

    return sig_transfer_ecc

def SigVerify(sig : bytearray, dtbs : bytearray, mycert : bytearray):

    with open(certdatei,"rb") as f:
        mycert2=f.read()

    cert = x509.load_pem_x509_certificate(mycert2, default_backend())
    public_key=cert.public_key()

    try:
        public_key.verify(sig, dtbs, ec.ECDSA(hashes.SHA256()) )
    except InvalidSignature:
        print("sig: fail")
        return False
    
    return True


@app.route('/', methods=['GET'])
@app.route('/login.py', methods=['GET', 'POST'])
def Connection_Handler():

    if request.method == 'GET':
        with open("/dev/urandom", "rb") as f:
            x=f.read(16)
        qr_output=io.BytesIO()
        challenge=b64encode(x).decode()
        # https://de.wikipedia.org/wiki/URL-Encoding
        challenge_url='tiauth://'+BaseURL+'?c='+challenge+'&a=0'
        if MY_DEBUG:
            return_url=challenge_url.replace('tiauth://','http://')
        else:
            return_url=challenge_url.replace('tiauth://','https://')
        CR.insert(ChallengeURL=challenge_url).execute()
        myqr = pyqrcode.create(challenge_url)
        myqr.svg(qr_output, scale=5)

        xx=qr_output.getvalue().decode()
        xx=re.sub("<\?xml[^>]+>\n","", xx)

        with open(certdatei, "rb") as f:
            tmp_cert=f.read()
        cert_transfer=tmp_cert.decode().replace("\n","\\n")


        return render_template('login.html', 
                               myqrcode=xx,
                               myqrdata=challenge_url,
                               myreturn_url=return_url,
                               # xxx, todo, Bei der Signatur müssen alle Daten
                               # rein (also zusätzliche Zertifikate)
                               mysig=Signatur(challenge_url.encode()).decode(),
                               mycert=cert_transfer
                              );
    elif request.method == 'POST':
        if request.content_type.startswith('application/json'):
            data=request.get_json()
            print(data)

            if 'QueryLoginStatus' in data:
                if (not 'CH_URL' in data) or (len(data['CH_URL'])>4000):
                    return jsonify({ "Result" : "NO" });
                x=CR.select(CR.ResponseOK).where(CR.ChallengeURL==data['CH_URL'])
                if len(x)==0:
                    return jsonify({ "Result" : "NO" });
                if x[0].ResponseOK:
                    # hier muss man dann den cookie setzen (Access-Token)
                    return jsonify({ "Result" : "Redirect" });
                else:
                    return jsonify({ "Result" : "NO" });

            if 'AuthSig' in data:
                for i in [ 'CH_URL', 'AUTCert', 'AuthSig']:
                    if (not i in data) or (len(data[i])>4000):
                        return Response('{ "Result" : "Failure, parameters." }', mimetype='application/json')
                
                if SigVerify(binascii.unhexlify(data['AuthSig']), data['CH_URL'].encode(), data['AUTCert'].encode()):
                        CR.update(Response=data['AuthSig'], ResponseOK=True).where(
                            CR.ChallengeURL==data['CH_URL']
                        ).execute()
                        return jsonify({ "Result" : "OK" })
                else:
                        return jsonify({ "Result" : "Failure, sigverify-failure." })

        return jsonify({ "Result" : "NO" });
    else:
        print(request.content_type);
        return jsonify({ "Result" : "HM????" });


    # End of ConnectionHandler()

@app.route('/ok.html', methods=['GET', 'POST'])
def Connection_Handler_ok_html():
    return render_template('ok.html')

@app.before_request
def before_request():
    Verbindung_CR_DB()

@app.teardown_request
def teardown_request(exception):
    Schliesse_CR_DB()

if __name__ == '__main__':

    for i in [ schlüsseldatei, certdatei, CR_DB_FILE ]:
        if not os.path.exists(i):
            sys.exit("Datei {} nicht gefunden.".format(i))

 
    MY_PORT=9000
    if not('HOSTNAME' in os.environ) or os.getenv('HOSTNAME')=='t':
        print("Produktiv-Server!")
        MY_DEBUG=False
        BaseURL="tiauth.gq/login.py"
    else:
        MY_DEBUG=True
        BaseURL="127.0.0.1:9000/login.py"
 
    app.run( port=MY_PORT, debug=MY_DEBUG )


#        return Response('Hallo, als VAU möchte ich gerne immer HTTP-POST-Request'+
#        'bekommen. Ich habe aber einen GET-Request bekommen -> Exit.\n',
#        mimetype='text/plain')
#
#    #print(request.content_type)
#    if request.content_type == 'application/json':
#        data_raw=request.get_data()
#        ClientHello_Hash=hashlib.sha256(data_raw).hexdigest()
#        #print(ClientHello_Hash)
#
#        data = request.get_json()
#
#        if data['MsgType']!="ClientHello":
#            print('Fehler erwarte ClientHello.\n')
#            return Response('{ "MsgType" : "ErrorMsg", "Msg" : "ClientHello expected"}',
#                   mimetype='application/json')
#
#        ClientHello=data
#        private_key=ec.generate_private_key(ec.BrainpoolP256R1(), default_backend())
#        public_key = private_key.public_key()
#        pn=public_key.public_numbers()
#        print("erzeuge ephemeres Schlüsselpaar ({}-Kurve)\n".format(pn.curve.name)+
#              "x={:x}\ny={:x}".format(pn.x,pn.y))
#        public_key_pem=public_key.public_bytes(serialization.Encoding.PEM,
#            serialization.PublicFormat.SubjectPublicKeyInfo).decode()
#
#        public_key_pem_j=public_key_pem.replace('\n','\\n')
#        ServerHello=\
#        '{"MsgType" : "ServerHello", "Ciphers" : [ "AES128GCMBrainpoolP256r1" ],'+\
#        ' "ClientHelloHash" : "'+ClientHello_Hash+'", "pubkey"  : "'+public_key_pem_j+'"}'
#
#        ServerHelloBase64=binascii.b2a_base64(ServerHello.encode(), newline=False)
#
#        # prüfe ich schon beim Start in main(), aber sicher ist sicher
#        assert os.path.exists(schlüsseldatei_ecc)
#
#        with open(schlüsseldatei_ecc, "rb") as f:
#            serialized_private_sig_key=f.read()
#
#        private_sig_key = serialization.load_pem_private_key(
#            serialized_private_sig_key,
#            password=None,
#            backend=default_backend()
#        )
#
#        signature = private_sig_key.sign( ServerHelloBase64 , ec.ECDSA(hashes.SHA256()) )
#        sig_transfer_ecc=binascii.hexlify(signature)
#
#        # prüfe ich schon beim Start in main(), aber sicher ist sicher
#        assert os.path.exists(schlüsseldatei_rsa)
#
#        with open(schlüsseldatei_rsa, "rb") as f:
#            serialized_private_sig_key=f.read()
#
#        private_sig_key = serialization.load_pem_private_key(
#            serialized_private_sig_key,
#            password=None,
#            backend=default_backend()
#        )
#
#        signature = private_sig_key.sign( 
#            ServerHelloBase64, 
#            padding.PSS(
#                mgf=padding.MGF1(hashes.SHA256()),
#                salt_length=padding.PSS.MAX_LENGTH
#            ),
#            hashes.SHA256()
#        )
#        sig_transfer_rsa=binascii.hexlify(signature)
#
#        assert os.path.exists(certdatei_ecc)
#        with open(certdatei_ecc, "rb") as f:
#            x=f.read()
#        vau_cert_ecc=x.decode().replace("\n","\\n")
#
#        assert os.path.exists(certdatei_rsa)
#        with open(certdatei_rsa, "rb") as f:
#            x=f.read()
#        vau_cert_rsa=x.decode().replace("\n","\\n")
#
#        ServerHelloSig=\
#        '{"MsgType" : "ServerHelloSig", "Data" : "'+ServerHelloBase64.decode()+'", "Signatures" : '+\
#        '{ "ECDSAwithSHA256" : "'+sig_transfer_ecc.decode()+'", "RSASSA-PSSwithSHA256" : "'+sig_transfer_rsa.decode()+'"'\
#        ' }, "Certificates" : [ "'+vau_cert_ecc+'", "'+vau_cert_rsa+'" ], "OCSPResponses" : [ ] }'
#
#        # Toll erledigt, jetzt geht es zur Schlüsselableitung
#        frontend_ephemeral_public_key = serialization.load_pem_public_key(
#            ClientHello["pubkey"].encode(),
#            backend=default_backend() 
#        )
#        frontend_ephemeral_pn=frontend_ephemeral_public_key.public_numbers()
#        print("Frontend ephemeraler öffentlicher ECDH-Schlüssel:\nx={:x}\ny={:x}\nKurve={}".format(
#                    frontend_ephemeral_pn.x, frontend_ephemeral_pn.y, frontend_ephemeral_pn.curve.name, ))
#
#        print("\nführe ECDH aus")
#        SharedSecret = private_key.exchange(ec.ECDH(), frontend_ephemeral_public_key)
#
#        print("SharedSecret:\n{}".format(binascii.hexlify(SharedSecret).decode()))
#
#        hkdf = HKDF(
#               algorithm=hashes.SHA256(), length=32,
#               salt=None, info=b'KeyID', backend=default_backend()
#        )
#        KeyID = hkdf.derive(SharedSecret)
#
#        print("Schlüsselableitung für die KeyID:\n{}".format(binascii.hexlify(KeyID).decode()))
#
#        hkdf = HKDF(
#               algorithm=hashes.SHA256(), length=16,
#               salt=None, info=b'AES128GCM-Key', backend=default_backend()
#        )
#        AESKey = hkdf.derive(SharedSecret)
#
#        print("Schlüsselableitung für AES128/GCM:\n{}".format(binascii.hexlify(AESKey).decode()))
#
#        q=Schluessel.insert(AESSchluessel=binascii.hexlify(AESKey), KeyID=binascii.hexlify(KeyID))
#        q.execute()
#
#        return Response(ServerHelloSig, mimetype='application/json')
#
#    elif request.content_type == 'application/octet-stream':
#        data_raw=request.get_data()
#
#        print("Data-length(raw):", len(data_raw))
#        if len(data_raw)<32+1+16:
#            ServerError='{"MsgType" : "ServerError", "Msg" : "Bad Enc-Data"}'
#            return Response(ServerError, mimetype='application/json')
#        
#        data=data_raw
#        KeyID=binascii.hexlify(data[0:32])
#        print("KeyID: ",KeyID)
#        x=Schluessel.select(Schluessel.AESSchluessel, Schluessel.MsgCounter).where(Schluessel.KeyID==KeyID)
#        assert len(x)<2
#        if len(x)==0:
#            ServerError='{"MsgType" : "ServerError", "Msg" : "KeyID ' + \
#                         KeyID.decode() +' not found"}'
#            return Response(ServerError, mimetype='application/json')
#
#        #print("XXX", x[0].AESSchluessel)
#        AESKey=binascii.unhexlify(x[0].AESSchluessel)
#        nonce=data[32:32+16]
#        data=data[32+16:]
#        aesgcm = AESGCM(AESKey)
#        try:
#            plaintext = aesgcm.decrypt(nonce, data, associated_data=None)
#        except InvalidTag:
#            return Response('{ "MsgType" : "ServerError", "Msg" : "AES/GCM Authentication Tag does not match."}',
#                   mimetype='application/json')
#            
#        MsgNr_data=plaintext[0:4]
#        MsgNr=int.from_bytes(MsgNr_data, byteorder='big', signed=False)
#        if not MsgNr>x[0].MsgCounter:
#            Schluessel.delete.where(Schuessel.KeyID==KeyID).execute()
#            return Response('{ "MsgType" : "ServerError", "Msg" : "MsgCounter too small."}',
#                   mimetype='application/json')
#
#        Schluessel.update(MsgCounter=MsgNr).where(Schluessel.KeyID==KeyID).execute()
#        plaintext=plaintext[4:]
#        print("Plaintext:", plaintext)
#
#        server_plaintext=MsgNr_data + b'Ich (Server) habe folgende Daten bekommen:' + plaintext
#        aesgcm = AESGCM(AESKey)
#        nonce = secrets.token_bytes(16)
#        data=binascii.unhexlify(KeyID)
#        #print("Data so far:", hexlify(data))
#        ciphertext = aesgcm.encrypt(nonce, server_plaintext, associated_data=None)
#        data=data+nonce+ciphertext
#        #print("Data so far({}):".format(len(data)), hexlify(data))
#
#        return Response(data, mimetype='application/octet-stream')
#
#    else:
#        print('Fehler Content-Type im POST-Request nicht gesetzt.\n')
#        return Response('{ "MsgType" : "ServerError", "Msg" : "No Content-Type in POST-Request"}',
#               mimetype='application/json')
#
